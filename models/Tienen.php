<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tienen".
 *
 * @property int $id
 * @property string|null $id_producto
 * @property string|null $id_consulta
 *
 * @property Consultas $consulta
 * @property ProductosAccesorios $producto
 */
class Tienen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tienen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() /*reglas que nos va a devolver y que debe de cumplir*/
    {
        return [
            [['id_producto'], 'required'], /*campo id obligatorio de rellenar*/
            [['id_consulta'], 'required'], /*campo id obligatorio de rellenar*/
            [['id_producto', ], 'string', 'max' => 6],/*campo con una longitud máxima de 6 caracteres*/
            [['id_producto'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan números'], /*campo id que unicamente nos va a permitir introducir números*/
            [['id_consulta'], 'string', 'max' => 7], /*campo id con una longitud máxima de 7 caracteres*/
            [['id_consulta'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan letras y números'], /*campo donde se aceptarán solo letras y números*/
            [['id_producto', 'id_consulta'], 'unique', 'targetAttribute' => ['id_producto', 'id_consulta']], /*campo único que no puede existir repetido*/
            [['id_consulta'], 'exist', 'skipOnError' => true, 'targetClass' => Consultas::class, 'targetAttribute' => ['id_consulta' => 'código']], /*campo único que no puede existir repetido*/
            [['id_producto'], 'exist', 'skipOnError' => true, 'targetClass' => ProductosAccesorios::class, 'targetAttribute' => ['id_producto' => 'id']], /*campo único que no puede existir repetido*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() /*atributos del modelo a mostrar*/
    {
        return [
            'id' => 'ID',
            'id_producto' => 'Id Producto',
            'id_consulta' => 'Id Consulta',
        ];
    }

    /**
     * Gets query for [[Consulta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConsulta()
    {
        return $this->hasOne(Consultas::class, ['código' => 'id_consulta']);
    }

    /**
     * Gets query for [[Producto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(ProductosAccesorios::class, ['id' => 'id_producto']);
    }
}
