<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "caracteristicas".
 *
 * @property int $id
 * @property string|null $id_raza
 * @property string|null $características
 *
 * @property Razas $raza
 */
class Caracteristicas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'caracteristicas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() /*clase que contiene las reglas del modelo*/
    {
        return [ /*reglas que nos va a devolver y que debe de cumplir*/
            [['id_raza'], 'required'], /*campo obligatorio de rellenar*/
            [['id_raza'], 'string', 'max' => 6 ], /*longitud máxima de caracteres de 6*/
            [['id_raza'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan letras y números'],/*está permitido utilizar números y letras mostrándose el mensaje por pantalla al usuario*/
            [['características'], 'string', 'max' => 100],/*longitud máxima de caracteres de 100*/
            [['características'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan letras y "," para separar'], /*solo se permite el uso de letras y comas para separar, además se le muestra el mensaje al usuario*/
            [['id_raza'], 'exist', 'skipOnError' => true, 'targetClass' => Razas::class, 'targetAttribute' => ['id_raza' => 'id']], /*en el caso de que la ID de raza esté duplicada mostrará un error de formulario*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() /*atributos del modelo a mostrar*/
    {
        return [
            'id' => 'ID',
            'id_raza' => 'Id Raza',
            'características' => 'Características',
        ];
    }

    /**
     * Gets query for [[Raza]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRaza()
    {
        return $this->hasOne(Razas::class, ['id' => 'id_raza']);
    }
    
 


}

