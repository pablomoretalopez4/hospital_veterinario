<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mascotas".
 *
 * @property string $código
 * @property int|null $edad
 * @property float|null $peso
 * @property string|null $color_pelo
 * @property string|null $nombre
 * @property string|null $código_consulta
 * @property string|null $nif_dueño
 *
 * @property Consultas $códigoConsulta
 * @property Duenos $nifDueño
 * @property Pertenecen[] $pertenecens
 * @property Razas[] $razas
 * @property Vacunas[] $vacunas
 */
class Mascotas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mascotas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() /*reglas que nos va a devolver y que debe de cumplir*/
    {
        return [
            [['código'], 'required'], /*campo obligatorio de rellenar*/
            [['código_consulta'], 'required',], /*campo obligatorio de rellenar*/
            [['código_consulta'], 'string', 'max'=>7], /*campo con una longitud máxima de 7 caracteres*/
            [['código_consulta'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan las letras y los números'],
            [['nif_dueño'], 'required'], /*campo obligatorio de rellenar*/
            [['nif_dueño'], 'string', 'max'=>9],  /*campo con una longitud máxima de 9 caracteres*/
            [['nif_dueño'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan las letras y los números'],  /*En el NIF del dueño unicamente nos va a permitir introducir letras y números*/
            [['edad'], 'integer', 'max' =>99],  /*campo con una longitud máxima de 99 caracteres*/
            [['peso'], 'number', 'max' =>999],  /*campo con una longitud máxima de 999 caracteres*/
            [['código'], 'string', 'max' => 3],  /*campo con una longitud máxima de 3 caracteres*/
            [['código'], 'match', 'pattern' => "/^[0-9]+$/", 'message' => 'Únicamente se aceptan números'], /*Únicamente se permitirá introducir números*/
            [['color_pelo', 'código_consulta', 'nif_dueño'], 'string', 'max' => 15],  /*Longitud máxima de 15 en cada uno de los 3 campos*/
            
            [['color_pelo'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan letras "," para separar y el signo "+"'],  /*En cuanto al color de pelo solo se permitirán letras, signo de la ',' y el '+' para separar.*/
            [['nombre'], 'string', 'max' => 15],  /*campo con una longitud máxima de 15 caracteres*/
            [['nombre'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan las letras y los números'],  /*Únicamente se aceptan letras y "," o "-" para separar*/
            [['código'], 'unique'], /*campo único que no puede existir repetido*/
            [['código_consulta'], 'exist', 'skipOnError' => true, 'targetClass' => Consultas::class, 'targetAttribute' => ['código_consulta' => 'código']], /*campo único que no puede existir repetido*/
            [['nif_dueño'], 'exist', 'skipOnError' => true, 'targetClass' => Duenos::class, 'targetAttribute' => ['nif_dueño' => 'nif']], /*campo único que no puede existir repetido*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() /*atributos del modelo a mostrar*/
    {
        return [
            'código' => 'Código',
            'edad' => 'Edad',
            'peso' => 'Peso',
            'color_pelo' => 'Color Pelo',
            'nombre' => 'Nombre',
            'código_consulta' => 'Código Consulta',
            'nif_dueño' => 'Nif Dueño',
        ];
    }

    /**
     * Gets query for [[CódigoConsulta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCódigoConsulta()
    {
        return $this->hasOne(Consultas::class, ['código' => 'código_consulta']);
    }

    /**
     * Gets query for [[NifDueño]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNifDueño()
    {
        return $this->hasOne(Duenos::class, ['nif' => 'nif_dueño']);
    }

    /**
     * Gets query for [[Pertenecens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPertenecens()
    {
        return $this->hasMany(Pertenecen::class, ['código_mascota' => 'código']);
    }

    /**
     * Gets query for [[Razas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRazas()
    {
        return $this->hasMany(Razas::class, ['id' => 'id_raza'])->viaTable('pertenecen', ['código_mascota' => 'código']);
    }

    /**
     * Gets query for [[Vacunas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVacunas()
    {
        return $this->hasMany(Vacunas::class, ['código_mascota' => 'código']);
    }
}

