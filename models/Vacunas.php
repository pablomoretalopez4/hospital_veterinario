<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacunas".
 *
 * @property string $código
 * @property string|null $tipo
 * @property float|null $cantidad_dosis
 * @property int|null $periodicidad
 * @property string|null $código_mascota
 *
 * @property Mascotas $códigoMascota
 */
class Vacunas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacunas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() /*reglas que nos va a devolver y que debe de cumplir*/
    {
        return [
            [['código'], 'required'], /*campo id obligatorio de rellenar*/
            [['código_mascota'], 'required'], /*campo id obligatorio de rellenar*/
            [['cantidad_dosis'], 'integer', 'max' => 999], /*campo cantidad de dosis con una longitud máxima de 999 caracteres*/
            [['periodicidad'], 'integer', 'max'=>999 ], /*campo periodicidad con una longitud máxima de 999 caracteres*/
            [['código_mascota'], 'string', 'max' => 3], /*campo código de mascota con una longitud máxima de 3 caracteres*/
            [['código'], 'string', 'max' => 6], /*campo código con una longitud máxima de 6 caracteres*/
            [['código'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan letras y números'], /*campo código donde solo se aceptan letras y números*/
            [['tipo'], 'string', 'max' => 15], /*campo tipo donde solo se acepta una longitud de 15 caracteres*/
            [['código'], 'unique'], /*campo único que no puede existir repetido*/
            [['código_mascota'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan números'], /*campo único que no puede existir repetido*/
            [['código_mascota'], 'exist', 'skipOnError' => true, 'targetClass' => Mascotas::class, 'targetAttribute' => ['código_mascota' => 'código']], /*campo único que no puede existir repetido*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() /*atributos del modelo a mostrar*/
    {
        return [
            'código' => 'Código',
            'tipo' => 'Tipo',
            'cantidad_dosis' => 'Cantidad Dosis',
            'periodicidad' => 'Periodicidad',
            'código_mascota' => 'Código Mascota',
        ];
    }

    /**
     * Gets query for [[CódigoMascota]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCódigoMascota()
    {
        return $this->hasOne(Mascotas::class, ['código' => 'código_mascota']);
    }
}
