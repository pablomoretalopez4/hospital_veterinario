<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "razas".
 *
 * @property string $id
 * @property string|null $tipo
 *
 * @property Caracteristicas[] $caracteristicas
 * @property Mascotas[] $códigoMascotas
 * @property Pertenecen[] $pertenecens
 */
class Razas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'razas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() /*reglas que nos va a devolver y que debe de cumplir*/
    {
        return [
            [['id'], 'required'], /*campo id obligatorio de rellenar*/
            [['id'], 'string', 'max' => 6], /*Longitud máxima de 6 caracteres*/
            [['id'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan letras y números'], /*En el id solo se aceptarán letras y números*/
            [['tipo'], 'string', 'max' => 15], /*Longitud máxima de 15 caracteres*/
            [['tipo'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan las letras'], /*En el tipo solo se permitirán letras*/
            [['id'], 'unique'], /*Campo único que no permite duplicados*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() /*atributos del modelo a mostrar*/
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * Gets query for [[Caracteristicas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCaracteristicas()
    {
        return $this->hasMany(Caracteristicas::class, ['id_raza' => 'id']);
    }

    /**
     * Gets query for [[CódigoMascotas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCódigoMascotas()
    {
        return $this->hasMany(Mascotas::class, ['código' => 'código_mascota'])->viaTable('pertenecen', ['id_raza' => 'id']);
    }

    /**
     * Gets query for [[Pertenecens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPertenecens()
    {
        return $this->hasMany(Pertenecen::class, ['id_raza' => 'id']);
    }
}
