<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $id
 * @property string|null $nif_dueños
 * @property int|null $teléfonos
 *
 * @property Duenos $nifDueños
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() /*reglas que nos va a devolver y que debe de cumplir*/
    {
        return [
            [['nif_dueños'], 'required'], /*Campo obligatorio de rellenar*/
            [['teléfonos'], 'required'], /*Campo obligatorio de rellenar*/
            [['teléfonos'], 'string', 'max' => 50], /*Campo con una longitud máxima de 50 caracteres*/
            [['teléfonos'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan números, "," para separar los teléfonos y el signo "+"'], /*Campo teléfonos en el que únicamente se van a aceptar números separados por comas y permitirá introducir el signo + para el prefijo del pais.*/
            [['nif_dueños'], 'string', 'max' => 9], /*Campo NIF dueños con una longitud máxima de 9 caracteres*/
            [['nif_dueños'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan letras y números'], /*En el campo del NIF unicamente se aceptarán letras y números*/
            [['nif_dueños'], 'exist', 'skipOnError' => true, 'targetClass' => Duenos::class, 'targetAttribute' => ['nif_dueños' => 'nif']], /*campo único que no puede existir repetido*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() /*atributos del modelo a mostrar*/
    {
        return [
            'id' => 'ID',
            'nif_dueños' => 'Nif Dueños',
            'teléfonos' => 'Teléfonos',
        ];
    }

    /**
     * Gets query for [[NifDueños]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNifDueños()
    {
        return $this->hasOne(Duenos::class, ['nif' => 'nif_dueños']);
    }
}
