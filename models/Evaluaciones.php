<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "evaluaciones".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $puntuacion
 * @property string|null $comentario
 * @property string $fecha
 */
class Evaluaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'evaluaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'], /*campo obligatorio de rellenar*/
            [['comentario'], 'required'], /*campo obligatorio de rellenar*/
            [['puntuacion'], 'integer'],
            [['comentario'], 'string'],
            [['fecha'], 'safe'],
            [['puntuacion',], 'integer', 'max' => 5],
            [['nombre'], 'string', 'max' => 50],
             [['comentario'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'puntuacion' => 'Puntuacion',
            'comentario' => 'Comentario',
            'fecha' => 'Fecha',
        ];
    }
}
