<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consultas".
 *
 * @property string $código
 * @property int|null $cantidad_pacientes
 *
 * @property Mascotas[] $mascotas
 * @property ProductosAccesorios[] $productos
 * @property Tienen[] $tienens
 */
class Consultas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() /*reglas que nos va a devolver y que debe de cumplir*/
    {
        return [
            [['código'], 'required'], /*campo obligatorio de rellenar*/
            [['cantidad_pacientes'], 'integer','message' => 'Únicamente se aceptan números', 'max' => 9],/*longitud máxima de caracteres de 9 y además solo nos va a permitir introducir números*/
            [['código'], 'string', 'max' => 9 ],  /*longitud máxima de caracteres de 7*/
            [['código'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan las letras y los números'], /*regla que unicamente nos va a permitir introducir letras y números*/
            [['código'], 'unique'], /*el código de cada consulta será único*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() /*atributos del modelo a mostrar*/
    {
        return [
            'código' => 'Código',
            'cantidad_pacientes' => 'Cantidad Pacientes',
        ];
    }

    /**
     * Gets query for [[Mascotas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMascotas()
    {
        return $this->hasMany(Mascotas::class, ['código_consulta' => 'código']);
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(ProductosAccesorios::class, ['id' => 'id_producto'])->viaTable('tienen', ['id_consulta' => 'código']);
    }

    /**
     * Gets query for [[Tienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::class, ['id_consulta' => 'código']);
    }
}
