<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pertenecen".
 *
 * @property int $id
 * @property string|null $código_mascota
 * @property string|null $id_raza
 *
 * @property Mascotas $códigoMascota
 * @property Razas $raza
 */
class Pertenecen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pertenecen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() /*reglas que nos va a devolver y que debe de cumplir*/
    {
        return [
            [['código_mascota','id_raza'], 'required'], /*Campos obligatorios de rellenar*/
            [['código_mascota'], 'string', 'max' => 3], /*Campo con una longitud máxima de 3 caracteres*/
            [['código_mascota'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan números'], /*Solo se permitirá introducir números*/
            [['id_raza'], 'string', 'max' => 6], /*El id de raza contará con una longitud máxima de 6 caracteres*/
            [['id_raza'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan letras y números'], /*Únicamente se permitirá introducir letras y números*/
            [['código_mascota', 'id_raza'], 'unique', 'targetAttribute' => ['código_mascota', 'id_raza']], /*Atributos únicos de un solo valor*/
            [['código_mascota'], 'exist', 'skipOnError' => true, 'targetClass' => Mascotas::class, 'targetAttribute' => ['código_mascota' => 'código']], /*campo único que no puede existir repetido*/
            [['id_raza'], 'exist', 'skipOnError' => true, 'targetClass' => Razas::class, 'targetAttribute' => ['id_raza' => 'id']],/*campo único que no puede existir repetido*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() /*atributos del modelo a mostrar*/
    {
        return [
            'id' => 'ID',
            'código_mascota' => 'Código Mascota',
            'id_raza' => 'Id Raza',
        ];
    }

    /**
     * Gets query for [[CódigoMascota]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCódigoMascota()
    {
        return $this->hasOne(Mascotas::class, ['código' => 'código_mascota']);
    }

    /**
     * Gets query for [[Raza]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRaza()
    {
        return $this->hasOne(Razas::class, ['id' => 'id_raza']);
    }
}
