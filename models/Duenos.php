<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "duenos".
 *
 * @property string $nif
 * @property string|null $calle
 * @property int|null $año_nacimiento
 * @property int|null $código_postal
 * @property string|null $ciudad
 * @property string|null $piso
 * @property string|null $provincia
 * @property int|null $portal
 *
 * @property Mascotas[] $mascotas
 * @property Telefonos[] $telefonos
 */
class Duenos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'duenos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() /*reglas que nos va a devolver y que debe de cumplir*/
    {
        return [
            [['nif'], 'required'], /*campo obligatorio de rellenar*/
            [['año_nacimiento','portal' ], 'integer', 'message' => 'Únicamente se aceptan números'], /*atributos en los que solo nos va a permitir introducir números*/
            [['año_nacimiento',], 'integer', 'max' => 2023], /*valor máximo año actual*/ 
            [['portal',], 'integer', 'max' => 300], /*valor máximo del portal 300*/
            [['código_postal',], 'integer', 'max' => 99999], /*valor máximo de CP 5 cifras*/
            [['nif'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan las letras y los números'],/*NIF solo acepta letras y números*/
            [['código_postal'], 'match', 'pattern' => "/^[a-z0-9]+$/", 'message' => 'Únicamente se aceptan números'], /*En el CP solo se permiten números*/
           
            [['nif'], 'string', 'max' => 9],/*El NIF tiene una longitud máxima de 9 caracteres*/
            [['calle'], 'string', 'max' => 40], /*La calle tiene una longitud máxima de 40 letras*/
            [['ciudad', 'provincia'], 'string', 'max' => 20], /*La ciudad t la provincia tienen una longitud máxima de 20*/
            [['ciudad', 'provincia'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan letras y "," para separar'],/*Solo se van a permitir letras y ',' como separación*/
            [['piso'], 'integer', 'max' => 12], /*El campo piso será un entero de 12 caracteres máximo*/
            [['piso'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan números'], /*Para rellenar el piso solo se utilizarán números*/
            [['nif'], 'unique'],/*El NIF será único*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    
   

    public function attributeLabels() /*atributos del modelo a mostrar*/
    {
        return [
            'nif' => 'Nif',
            'calle' => 'Calle',
            'año_nacimiento' => 'Año Nacimiento',
            'código_postal' => 'Código Postal',
            'ciudad' => 'Ciudad',
            'piso' => 'Piso',
            'provincia' => 'Provincia',
            'portal' => 'Portal',
        ];
    }

    /**
     * Gets query for [[Mascotas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMascotas()
    {
        return $this->hasMany(Mascotas::class, ['nif_dueño' => 'nif']);
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::class, ['nif_dueños' => 'nif']);
    }
}
