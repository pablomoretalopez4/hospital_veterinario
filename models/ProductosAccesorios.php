<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos_accesorios".
 *
 * @property string $id
 * @property string|null $tipo
 * @property string|null $descripcion
 *
 * @property Consultas[] $consultas
 * @property Tienen[] $tienens
 */
class ProductosAccesorios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos_accesorios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()/*reglas que nos va a devolver y que debe de cumplir*/
    {
        return [
            [['id'], 'required'], /*campo id obligatorio de completar*/
            [['id'], 'string', 'max' => 6], /*Longitud máxima de 6 caracteres*/
            [['id'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan números'], /*ID que solo nos va a permitir introducir números*/
            [['tipo'], 'string', 'max' => 27], /*Campo tipo con una longitud máxima de 27 caracteres*/
            [['tipo'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan letras, "," para separar y el signo "+"'], /*El campo tipo solo permitirá el uso de letras y como métodos de separación la ',' y el signo '+' */
            [['descripcion'], 'string', 'max' => 50], /*Descripción con una longitud de 50 caracteres como máximo*/
            [['descripcion'], 'match', 'pattern' => "/^[a-zA-Z0-9]+$/", 'message' => 'Únicamente se aceptan letras y "," o "-" para separar'], /*El campo descripción solo permitirá el uso de letras y como métodos de separación la ',' y el '-' */
            [['id'], 'unique'], /*campo único que no admite duplicados*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() /*atributos del modelo a mostrar*/
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Consultas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConsultas()
    {
        return $this->hasMany(Consultas::class, ['código' => 'id_consulta'])->viaTable('tienen', ['id_producto' => 'id']);
    }

    /**
     * Gets query for [[Tienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::class, ['id_producto' => 'id']);
    }
}
