-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-09-2023 a las 08:02:10
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 7.3.5
DROP DATABASE IF EXISTS hospital_veterinario;
CREATE DATABASE IF NOT EXISTS hospital_veterinario
CHARACTER SET utf8
COLLATE utf8_spanish_ci;
USE hospital_veterinario;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hospital_veterinario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracteristicas`
--

CREATE TABLE `caracteristicas` (
  `id` int(11) NOT NULL,
  `id_raza` varchar(9) DEFAULT NULL,
  `características` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `caracteristicas`
--

INSERT INTO `caracteristicas` (`id`, `id_raza`, `características`) VALUES
(23001, '85633A', 'Castrado'),
(37562, '72829S', 'Pelaje claro'),
(41415, '26624F', 'Albino '),
(41587, '85426E', 'Hipertiroideo'),
(42596, '85426E', 'Dieta Barf'),
(45211, '55962W', 'Alérgico a los cereales '),
(52148, '63251L', 'Hipotiroideo'),
(73832, '72829S', 'Alimentación variada'),
(77895, '85525J', 'Asmático'),
(82912, '72829S', 'Dieta variada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultas`
--

CREATE TABLE `consultas` (
  `código` varchar(9) NOT NULL,
  `cantidad_pacientes` int(1) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1489 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `consultas`
--

INSERT INTO `consultas` (`código`, `cantidad_pacientes`) VALUES
('087680S', 5),
('127821A', 6),
('172901H', 1),
('172999W', 1),
('208717B', 6),
('301123R', 3),
('415268L', 4),
('425689S', 3),
('451644P', 2),
('515409M', 6),
('520015O', 5),
('569659W', 4),
('627829A', 1),
('627829B', 2),
('627829J', 2),
('654897N', 7),
('668191Z', 5),
('706515D', 10),
('785266Ñ', 9),
('840498E', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `duenos`
--

CREATE TABLE `duenos` (
  `nif` varchar(9) NOT NULL,
  `calle` varchar(40) DEFAULT NULL,
  `año_nacimiento` int(5) DEFAULT NULL,
  `código_postal` int(6) DEFAULT NULL,
  `ciudad` varchar(20) DEFAULT NULL,
  `piso` varchar(15) DEFAULT NULL,
  `provincia` varchar(20) DEFAULT NULL,
  `portal` int(5) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `duenos`
--

INSERT INTO `duenos` (`nif`, `calle`, `año_nacimiento`, `código_postal`, `ciudad`, `piso`, `provincia`, `portal`) VALUES
('00218425D', 'Leonardo Rucabado', 1995, 39700, 'Castro', '6', 'Cantabria', 18),
('12892919F', 'Avenida Cantabria', 2004, 39005, 'Santander', '4', 'Cantabria', 34),
('15827653F', 'Cervantes', 2000, 39005, 'Santander', '1', 'Cantabria', 110),
('16545675P', 'Vela Mayor', 2002, 39700, 'Castro', '5', 'Cantabria', 32),
('36717611G', 'San Fernando', 2001, 39006, 'Santander', '8', 'Cantabria', 1),
('36717611S', 'General Dávila', 1990, 39006, 'Santander', '3', 'Cantabria', 20),
('51260445T', 'Santa María', 2004, 48550, 'Muskiz', '5', 'Bizkaia', 10),
('51651515E', 'Lauaxeta', 1955, 48550, 'Muskiz', '2', 'Bizkaia', 3),
('52554866S', 'La Rúa', 1970, 39700, 'Castro-Urdiales', '2', 'Cantabria', 22),
('55865551D', 'General Dávila', 1998, 39004, 'Santander', '7', 'Cantabria', 240),
('61251525W', 'Urbanización el cantábrico', 1970, 39709, 'Castro-Urdiales', '4', 'Cantabria', 12),
('66631671H', 'Honduras', 1988, 39007, 'Santander', '4', 'Cantabria', 13),
('66631671P', 'Honduras', 1999, 39006, 'Santander', '2', 'Cantabria', 23),
('73658444K', 'La Paz', 1995, 39700, 'Castro-Urdiales', '1', 'Cantabria', 13),
('75154568Z', 'Juan de la Cosa', 1969, 39700, 'Castro-Urdiales', '5', 'Cantabria', 50),
('82929199S', 'Albert Einstein', 1978, 39006, 'Santander', '2', 'Cantabria', 40),
('85616512M', 'Ardigales', 1992, 39700, 'Castro-Urdiales', '3', 'Cantabria', 40),
('86392911F', 'Los Castros', 1991, 39005, 'Santander', '3', 'Cantabria', 67),
('96584125F', 'La mar', 2000, 39700, 'Castro-Urdiales', '1', 'Cantabria', 10),
('99992911K', 'Los Castros', 1999, 39005, 'Santander', '5', 'Cantabria', 167);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluaciones`
--

CREATE TABLE `evaluaciones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `puntuacion` int(11) DEFAULT NULL,
  `comentario` text DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `evaluaciones`
--

INSERT INTO `evaluaciones` (`id`, `nombre`, `puntuacion`, `comentario`, `fecha`) VALUES
(1, 'Pablo', 4, 'Excelente servicio, lo recomiendo totalmente.', '2023-07-27 13:11:04'),
(2, 'María', 5, 'Increíble experiencia, el personal es muy amable.', '2023-07-27 13:11:04'),
(3, 'Pedro', 3, 'Buen lugar, pero podrían mejorar en la atención al cliente.', '2023-07-27 13:11:04'),
(17, 'Juan', 5, 'Recomendado totalmente, un ambiente muy familiar desde que entras por la puerta.', '2023-08-14 22:49:23'),
(18, 'Sofía', 5, 'Un sitio fantástico y muy profesional. Gracias a Susana especialmente, que trato a nuestro gato increíblemente.', '2023-08-14 22:55:46'),
(19, 'Alex', 2, 'El servicio de teleconsulta no cumplió mis expectativas.', '2023-08-15 02:32:29'),
(20, 'Martín', 4, 'Buena limpieza en el hospital, especialmente en las consultas y en la sala de espera.', '2023-08-18 17:25:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mascotas`
--

CREATE TABLE `mascotas` (
  `código` varchar(9) NOT NULL,
  `edad` int(3) DEFAULT NULL,
  `peso` decimal(5,0) DEFAULT NULL,
  `color_pelo` varchar(9) DEFAULT NULL,
  `nombre` varchar(15) DEFAULT NULL,
  `código_consulta` varchar(9) DEFAULT NULL,
  `nif_dueño` varchar(9) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `mascotas`
--

INSERT INTO `mascotas` (`código`, `edad`, `peso`, `color_pelo`, `nombre`, `código_consulta`, `nif_dueño`) VALUES
('123', 2, '2', '2', '2', '627829A', '66631671H'),
('129', 2, '30', 'marron', 'Poleo', '208717B', '55865551D'),
('147', 1, '3', 'blanco', 'Thor', '515409M', '36717611G'),
('189', 11, '31', 'marrón', 'Kira', '627829J', '82929199S'),
('222', 2, '19', 'marrón', 'Leo', '627829J', '36717611G'),
('235', 5, '8', 'gris', 'Bambú', '425689S', '51651515E'),
('302', 6, '20', 'negro', 'Toby', '627829B', '96584125F'),
('330', 7, '29', 'marrón', 'Luna', '172999W', '55865551D'),
('447', 10, '15', 'negro', 'Lila', '627829A', '51260445T'),
('456', 4, '5', 'gris', 'Milo', '668191Z', '55865551D'),
('471', 6, '8', 'blanco', 'Wanda', '654897N', '85616512M'),
('511', 11, '10', 'marrón', 'Dunya', '668191Z', '51651515E'),
('558', 14, '41', 'marrón', 'Menta', '668191Z', '15827653F'),
('562', 8, '16', 'marrón', 'Max', '087680S', '00218425D'),
('662', 1, '1', 'blanco', 'Coco', '425689S', '51651515E'),
('666', 13, '21', 'marrón', 'Rayo', '172999W', '86392911F'),
('768', 4, '10', 'marrón', 'Rayo', '301123R', '86392911F'),
('881', 4, '9', 'blanco', 'Queen', '627829J', '36717611G'),
('987', 2, '11', 'marrón', 'Rocky', '172901H', '36717611S');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pertenecen`
--

CREATE TABLE `pertenecen` (
  `id` int(11) NOT NULL,
  `código_mascota` varchar(9) DEFAULT NULL,
  `id_raza` varchar(9) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `pertenecen`
--

INSERT INTO `pertenecen` (`id`, `código_mascota`, `id_raza`) VALUES
(56, '768', '72829S');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_accesorios`
--

CREATE TABLE `productos_accesorios` (
  `id` varchar(9) NOT NULL,
  `tipo` varchar(20) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `productos_accesorios`
--

INSERT INTO `productos_accesorios` (`id`, `tipo`, `descripcion`) VALUES
('162829', 'juguete', 'Mordedor'),
('411626', 'accesorio', 'Peluche'),
('413925', 'medicamento', 'Canitenol'),
('425159', 'medicamento', 'Otican'),
('451451', 'accesorio', 'Hills húmedo gato'),
('452109', 'accesorio', 'Arnés perro'),
('455921', 'accesorio', 'Correa para perro'),
('521006', 'producto', 'Pouches para gato'),
('526006', 'producto', 'Hills dieta perro'),
('547292', 'juguete', 'Pelota'),
('571006', 'medicamento', 'Bravecto'),
('578962', 'medicamento', 'Gastrovet'),
('649201', 'medicamento', 'Amoxicilina'),
('672819', 'medicamento', 'Nexgard '),
('728282', 'medicamento', 'Tobrex'),
('737812', 'juguete', 'Hueso'),
('739919', 'medicamento', 'Prednisona'),
('781301', 'medicamento', 'Apoquel'),
('872727', 'accesorio', 'Afeitadora'),
('997292', 'medicamento', 'Oleoderm');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `razas`
--

CREATE TABLE `razas` (
  `id` varchar(9) NOT NULL,
  `tipo` varchar(15) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1489 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `razas`
--

INSERT INTO `razas` (`id`, `tipo`) VALUES
('26624F', 'Cobaya'),
('41250O', 'Border collie'),
('45210P', 'Persa'),
('48264Q', 'Bulldog francés'),
('54789L', 'Pomerania'),
('55962W', 'British'),
('56515M', 'Potoka'),
('63215N', 'Tortuga'),
('63251L', 'Loro'),
('65872M', 'Schnauzer'),
('72829S', 'Reptil'),
('76214C', 'Golden retrieve'),
('76321V', 'Pekinés'),
('76951B', 'Bichón maltés'),
('84984B', 'Árabe'),
('85426E', 'Labrador'),
('85525J', 'Siamés'),
('85623D', 'Jerbo'),
('85633A', 'Canario'),
('94525G', 'Goldfish');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefonos`
--

CREATE TABLE `telefonos` (
  `id` int(11) NOT NULL,
  `nif_dueños` varchar(9) DEFAULT NULL,
  `teléfonos` int(15) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `telefonos`
--

INSERT INTO `telefonos` (`id`, `nif_dueños`, `teléfonos`) VALUES
(111, '66631671H', 562718991),
(9872, '86392911F', 562718991),
(284749, '55865551D', 888882727),
(1145265, '36717611G', 652114856),
(1458925, '75154568Z', 658774411),
(1548756, '73658444K', 685471239),
(1672811, '12892919F', 659391911),
(2457841, '55865551D', 652148564),
(2554896, '75154568Z', 630332587),
(4152630, '16545675P', 652200140),
(4156284, '12892919F', 699874251),
(4785126, '55865551D', 658746514),
(5437281, '12892919F', 872727171),
(5842187, '86392911F', 600147930),
(6398201, '99992911K', 187198822),
(6541289, '82929199S', 655441207),
(7654321, '66631671H', 817098791),
(7771717, '55865551D', 721891289),
(8171811, '55865551D', 872727171),
(8292919, '55865551D', 882727727);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienen`
--

CREATE TABLE `tienen` (
  `id` int(11) NOT NULL,
  `id_producto` varchar(9) DEFAULT NULL,
  `id_consulta` varchar(9) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `tienen`
--

INSERT INTO `tienen` (`id`, `id_producto`, `id_consulta`) VALUES
(147, '411626', '654897N'),
(852, '425159', '627829A'),
(412, '425159', '654897N'),
(521, '425159', '706515D'),
(963, '451451', '515409M'),
(845, '452109', '087680S'),
(201, '452109', '627829B'),
(142, '452109', '668191Z'),
(698, '526006', '172999W'),
(123, '547292', '087680S'),
(767, '547292', '172999W'),
(745, '547292', '208717B'),
(369, '547292', '627829J'),
(102, '578962', '208717B'),
(799, '672819', '627829B'),
(766, '728282', '208717B'),
(999, '728282', '301123R'),
(661, '728282', '668191Z'),
(567, '737812', '301123R'),
(716, '997292', '172999W');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(250) NOT NULL,
  `authKey` varchar(250) NOT NULL,
  `accessToken` varchar(250) NOT NULL,
  `activate` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `authKey`, `accessToken`, `activate`) VALUES
(1, 'pruebapablo', 'pruebapablo@gmail.com', 'fsAzed3.rqWhQ', '7df25146a3da320d8cead85601898cf1ca7d14a99b001f81791ab59feff160c2cd491bf226ec70b0468e7aaaabff9b67e761b3da775b7a76b0af32baabecc180c7aee0e5bff62a38b8296b10ccb398b77cda89650fb8fe51a0d667b068e321e26e086956', 'b6130b22721d2deada15baf2ac44c75e7cda07da647e0c4a830f9f2e5900c85d48513e6d77d0f3a09f4eceefb981438fa625f1386afc6123f95008ae4a79dbb3a93de6749eb4ac620937cce5e9cd76aef2b8d13d5b049422a898fa272344f2732847a7ed', 0),
(13, 'Pablo', 'pablo.moreta.lopez@gmail.com', 'fsAzed3.rqWhQ', '4309188c6af2d5a7a3c5c229818ca49bc4f2489bff5a1b6185cc45f37b66270cac0b2560a0e98d665339dff0d132b88a85b112a5a48c1d8f1190f9e38f4add74b4756d923210eca91e337e3a4114bd09fd62f7838cdaa0077228143722ef8643be966321', 'e8528274b9dae60ce73a912ac53c3c7b4ae1bceb2ac77a21fc21dcd47a050e755295ecf6d83ad9c47f48c88305152eca246f5468ab30bfc31912005a478da8bad8c733910d30ced2e9fff7cc4b9d24d783b23e8f15304cd67166d856b37ab4b891e7116e', 0),
(14, 'Yaiza', 'yaiza@gmail.com', 'fs55wA4ZMPOR.', '41bda2e3eee762e503aa39004d3956ee3dcf9c9530da3c297cb6888fcdca7a7028c0a0b624fb6e9ff4c8dcaa088b876db6b0b3b29a2a51afae39f8eb89dcb6fac301cb91ae1a846699255dd4d06d6bfab8b5e0afb2a1fa2e389f4b6fd2cc0474bb52abe4', '85e7337e203adeb5ba55d3b9888379bcbea5775af283e529f1aaff0b7fe7d6f4c373258f256fa7f6538f193bd0934167de98f8aa4e93b3b2c3a7fb68adad39fe1d25b91e611b820e0093f34523f41cbf0ea3967e73464ba5323201f0a76dc8cbdc1bb226', 0),
(15, 'Daniel', 'daniel@gmail.com', 'fsl0YfyaZuKqE', '85644f2da7b1d76697466f0438bad4ec33c4b9f84727ae54d347d6e45bc52b01408d344f16ffcc7474131b131642e64191ab01740d85e8f5c3682dd3938dc8b7fe74c489d933fe5972b37cc95c15086912d705084077c8431d966a11ab4d97a373a5ab78', '28d48c6e3def5b6abdfa68d13edca07e8842692d88ec07cbbdccdac7388a33cd396e9d2bc8a5b93e288a46b2eeadaabd483627d0f59f9858a03ca2f366c9978fed7895e68ced7859cfc3267fade038aad7e2c8dc524b0ef34206cd7eda629804f7afa126', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vacunas`
--

CREATE TABLE `vacunas` (
  `código` varchar(9) NOT NULL,
  `tipo` varchar(15) DEFAULT NULL,
  `cantidad_dosis` double DEFAULT NULL,
  `periodicidad` int(11) DEFAULT NULL,
  `código_mascota` varchar(9) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `vacunas`
--

INSERT INTO `vacunas` (`código`, `tipo`, `cantidad_dosis`, `periodicidad`, `código_mascota`) VALUES
('14145A', 'Adenitis', 9, 1, '129'),
('16712G', 'Hepatitis', 1, 2, '330'),
('21458D', 'Octovalente', 5, 2, '987'),
('22547F', 'Hexavalente', 7, 1, '129'),
('23154C', 'Tétanos', 4.3, 4, '558'),
('25842L', 'Leishmaniosis', 3.5, 2, '768'),
('28282F', 'Moquillo', 4.3, 3, '330'),
('51489K', 'Parvigen', 8.5, 1, '330'),
('52148P', 'Trivalente', 5, 1, '666'),
('558', 'Rabia', 2, 2, '129'),
('65875I', 'Tetravalente', 2.5, 2, '222'),
('71718A', 'Parvovirosis', 1, 1, '666'),
('72626K', 'Lyme', 1.5, 3, '129'),
('72718H', 'Coronavirus', 2.5, 1, '987'),
('76181S', 'Rabia', 3, 4, '129'),
('83919P', 'Hepatitis', 4.2, 2, '987'),
('92828F', 'Peritonitis', 5, 2, '222');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `caracteristicas`
--
ALTER TABLE `caracteristicas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_características_raza` (`id_raza`);

--
-- Indices de la tabla `consultas`
--
ALTER TABLE `consultas`
  ADD PRIMARY KEY (`código`);

--
-- Indices de la tabla `duenos`
--
ALTER TABLE `duenos`
  ADD PRIMARY KEY (`nif`);

--
-- Indices de la tabla `evaluaciones`
--
ALTER TABLE `evaluaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mascotas`
--
ALTER TABLE `mascotas`
  ADD PRIMARY KEY (`código`),
  ADD KEY `fk_mascotas_consultas` (`código_consulta`),
  ADD KEY `fk_mascotas_dueños` (`nif_dueño`);

--
-- Indices de la tabla `pertenecen`
--
ALTER TABLE `pertenecen`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_mascotas_razas` (`código_mascota`,`id_raza`),
  ADD KEY `fk_pertenecen_razas` (`id_raza`);

--
-- Indices de la tabla `productos_accesorios`
--
ALTER TABLE `productos_accesorios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `razas`
--
ALTER TABLE `razas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_telefono_dueños` (`nif_dueños`);

--
-- Indices de la tabla `tienen`
--
ALTER TABLE `tienen`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_producto_consultas` (`id_producto`,`id_consulta`),
  ADD KEY `fk_tienen_consultas` (`id_consulta`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vacunas`
--
ALTER TABLE `vacunas`
  ADD PRIMARY KEY (`código`),
  ADD KEY `fk_vacunas_mascotas` (`código_mascota`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `caracteristicas`
--
ALTER TABLE `caracteristicas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98928;

--
-- AUTO_INCREMENT de la tabla `evaluaciones`
--
ALTER TABLE `evaluaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `pertenecen`
--
ALTER TABLE `pertenecen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8292921;

--
-- AUTO_INCREMENT de la tabla `tienen`
--
ALTER TABLE `tienen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `caracteristicas`
--
ALTER TABLE `caracteristicas`
  ADD CONSTRAINT `fk_características_raza` FOREIGN KEY (`id_raza`) REFERENCES `razas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mascotas`
--
ALTER TABLE `mascotas`
  ADD CONSTRAINT `fk_mascotas_consultas` FOREIGN KEY (`código_consulta`) REFERENCES `consultas` (`código`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mascotas_dueños` FOREIGN KEY (`nif_dueño`) REFERENCES `duenos` (`nif`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pertenecen`
--
ALTER TABLE `pertenecen`
  ADD CONSTRAINT `fk_pertenecen_mascotas` FOREIGN KEY (`código_mascota`) REFERENCES `mascotas` (`código`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pertenecen_razas` FOREIGN KEY (`id_raza`) REFERENCES `razas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD CONSTRAINT `fk_telefono_dueños` FOREIGN KEY (`nif_dueños`) REFERENCES `duenos` (`nif`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tienen`
--
ALTER TABLE `tienen`
  ADD CONSTRAINT `fk_tienen_consultas` FOREIGN KEY (`id_consulta`) REFERENCES `consultas` (`código`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tienen_productos` FOREIGN KEY (`id_producto`) REFERENCES `productos_accesorios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vacunas`
--
ALTER TABLE `vacunas`
  ADD CONSTRAINT `fk_vacunas_mascotas` FOREIGN KEY (`código_mascota`) REFERENCES `mascotas` (`código`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
