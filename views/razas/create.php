<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Razas $model */

$this->title = 'Crear Raza';
$this->params['breadcrumbs'][] = ['label' => 'Razas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="razas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
