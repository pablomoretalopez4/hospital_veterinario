<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Vacunas $model */

$this->title = 'Actualizar Vacuna: ' . $model->código;
$this->params['breadcrumbs'][] = ['label' => 'Vacunas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->código, 'url' => ['view', 'código' => $model->código]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vacunas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
