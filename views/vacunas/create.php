<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Vacunas $model */

$this->title = 'Crear Vacuna';
$this->params['breadcrumbs'][] = ['label' => 'Vacunas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacunas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
