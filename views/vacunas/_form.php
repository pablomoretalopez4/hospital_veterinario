<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Vacunas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="vacunas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'código')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cantidad_dosis')->textInput() ?>

    <?= $form->field($model, 'periodicidad')->textInput() ?>

    
    <?= $form->field($model, 'código_mascota')->dropDownList(ArrayHelper::map(\app\models\Mascotas::find()->all(), 'código', 'código'),
    ['prompt' => 'Selecciona una mascota']) ?>
    
  


    


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
