<?php

use app\models\Evaluaciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Evaluaciones';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="evaluaciones-index">

    <center><h1><?= Html::encode($this->title) ?></h1></center>

    <p>
        <?= Html::a(' Añadir un comentario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


  <?php


// Conexión a la base de datos
$conexion = mysqli_connect("localhost", "root", "", "hospital_veterinario");

// Consulta para obtener las evaluaciones y reseñas
$query = "SELECT * FROM evaluaciones ORDER BY fecha DESC";
$resultado = mysqli_query($conexion, $query);

// Mostrar las evaluaciones y reseñas
while ($fila = mysqli_fetch_assoc($resultado)) {
    echo "<p><strong>{$fila['nombre']}</strong></p>";
    echo "<p>Puntuación: {$fila['puntuacion']}</p>";
    echo "<p>Comentario: {$fila['comentario']}</p>";
    echo "<hr>";
}

// Cerrar conexión a la base de datos
mysqli_close($conexion);
?>


</div>
