<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Duenos $model */

$this->title = 'Crear Dueño';
$this->params['breadcrumbs'][] = ['label' => 'Duenos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="duenos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
