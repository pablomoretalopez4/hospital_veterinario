<?php

use app\models\Duenos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Dueños';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (!Yii::$app->user->isGuest): ?>
    <div id="fondo">
<div class="duenos-index">

    <center><h1><?= Html::encode($this->title) ?></h1></center><br>

    <p>
        <?= Html::a('Crear Dueño', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            /**['class' => 'yii\grid\SerialColumn'],*/

            'nif',
            'calle',
            'año_nacimiento',
            'código_postal',
            'ciudad',
            //'piso',
            //'provincia',
            //'portal',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Duenos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'nif' => $model->nif]);
                 }
            ],
        ],
    ]); ?>


</div>
</div>
<?php else: ?>
   <?php
   $mensaje = '<div class="alert alert-warning" style="background-color: #E96363; color: white;">Debes iniciar sesión para tener acceso a esta página</div>';

    echo $mensaje;
?>
<?php endif; ?>

