<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Duenos $model */

$this->title = $model->nif;
$this->params['breadcrumbs'][] = ['label' => 'Duenos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="duenos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'nif' => $model->nif], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'nif' => $model->nif], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que deseas eliminar este campo?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nif',
            'calle',
            'año_nacimiento',
            'código_postal',
            'ciudad',
            'piso',
            'provincia',
            'portal',
        ],
    ]) ?>

</div>
