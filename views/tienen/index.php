<?php

use app\models\Tienen;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Tienen';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (!Yii::$app->user->isGuest): ?>
    <div id="fondo">
<div class="tienen-index">

    <center><h1><?= Html::encode($this->title) ?></h1></center><br>

    <p>
        <?= Html::a('Crear Tienen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
           /* ['class' => 'yii\grid\SerialColumn'],*/

            'id',
            'id_producto',
            'id_consulta',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Tienen $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
</div><!-- comment -->
<?php else: ?>
    <?php
   $mensaje = '<div class="alert alert-warning" style="background-color: #E96363; color: white;">Debes iniciar sesión para tener acceso a esta página</div>';

    echo $mensaje;
?>
<?php endif; ?>

