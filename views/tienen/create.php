<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Tienen $model */

$this->title = 'Crear Tienen';
$this->params['breadcrumbs'][] = ['label' => 'Tienens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tienen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
