<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Tienen $model */

$this->title = 'Actualizar Tienen: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tienens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tienen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
