<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Tienen $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="tienen-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'id_producto')->dropDownList(
        ArrayHelper::map(app\models\ProductosAccesorios::find()->all(), 'id', 'id'),
        ['prompt'=>'Selecciona un producto']
    );
?>
    <?= $form->field($model, 'id_consulta')->dropDownList(
        ArrayHelper::map(app\models\Consultas::find()->all(), 'código', 'código'),
        ['prompt'=>'Selecciona una consulta']
    );
?>

    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
