<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Consultas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="consultas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'código')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cantidad_pacientes')->textInput()?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
