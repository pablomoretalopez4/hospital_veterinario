<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Consultas $model */

$this->title = 'Actualizar Consulta: ' . $model->código;
$this->params['breadcrumbs'][] = ['label' => 'Consultas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->código, 'url' => ['view', 'código' => $model->código]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="consultas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
