<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ProductosAccesorios $model */

$this->title = 'Crear Producto / Accesorio';
$this->params['breadcrumbs'][] = ['label' => 'Productos Accesorios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-accesorios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
