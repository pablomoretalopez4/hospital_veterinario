<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ProductosAccesorios $model */

$this->title = 'Actualizar Producto o Accesorio: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Productos Accesorios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="productos-accesorios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
