<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Caracteristicas $model */

$this->title = 'Actualizar Característica: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Caracteristicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="caracteristicas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
