<?php

use app\models\Caracteristicas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Características';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (!Yii::$app->user->isGuest): ?>
    <div id="fondo"> <!--Coger las características de la clase fondo-->
   
<div class="caracteristicas-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center><br>

    <p>
        <?= Html::a('Crear Característica', ['create'], ['class' => 'btn btn-success']) ?> <!--Botón que nos va a permitir crear-->
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
           /** ['class' => 'yii\grid\SerialColumn'],*/

            'id',
            'id_raza',
            'características',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Caracteristicas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
    </div>
<?php else: ?>
    <?php
   $mensaje = '<div class="alert alert-warning" style="background-color: #E96363; color: white;">Debes iniciar sesión para tener acceso a esta página</div>';

    echo $mensaje;
?>
<?php endif; ?>


