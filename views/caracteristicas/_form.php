<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/** @var yii\web\View $this */
/** @var app\models\Caracteristicas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="caracteristicas-form">
    
    
   
    <?php $form = ActiveForm::begin(); ?>

    
    
   <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>
      <?= $form->field($model, 'id_raza')->dropDownList(
        ArrayHelper::map(app\models\Razas::find()->all(), 'id', 'id'),
        ['prompt'=>'Selecciona una Raza']
    );
?>
     
   

   

    

    <?= $form->field($model, 'características')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
