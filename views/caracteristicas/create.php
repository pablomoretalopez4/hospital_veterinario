<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Caracteristicas $model */

$this->title = 'Crear Característica';
$this->params['breadcrumbs'][] = ['label' => 'Caracteristicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="caracteristicas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        
    ]) ?>

</div>
