<?php
use yii\helpers\ArrayHelper;

use yii\helpers\Html;

use kartik\select2\Select2;
use yii\widgets\ActiveForm;
/** @var yii\web\View $this */
/** @var app\models\Mascotas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="mascotas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'código')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'edad')->textInput() ?>

    <?= $form->field($model, 'peso')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color_pelo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    
  

    <?= $form->field($model, 'código_consulta')->dropDownList(
        ArrayHelper::map(app\models\Consultas::find()->all(), 'código', 'código'),
        ['prompt'=>'Selecciona una Consulta']
    );
?>
    
    
    

    
    <?= $form->field($model, 'nif_dueño')->dropDownList(
        ArrayHelper::map(app\models\Duenos::find()->all(), 'nif', 'nif'),
        ['prompt'=>'Selecciona un dueño']
    );
?>

    

    

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
