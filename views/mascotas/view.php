<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Mascotas $model */

$this->title = $model->código;
$this->params['breadcrumbs'][] = ['label' => 'Mascotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mascotas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'código' => $model->código], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'código' => $model->código], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que deseas eliminar este campo?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'código',
            'edad',
            'peso',
            'color_pelo',
            'nombre',
            'código_consulta',
            'nif_dueño',
        ],
    ]) ?>

</div>

