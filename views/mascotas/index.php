<?php

use app\models\Mascotas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Mascotas';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (!Yii::$app->user->isGuest): ?>
   <div id="fondo">
<div class="mascotas-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center><br>

    <p>
        <?= Html::a('Crear Mascota', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
           /** ['class' => 'yii\grid\SerialColumn'],*/

            'código',
            'edad',
            'peso',
            'color_pelo',
            'nombre',
            //'código_consulta',
            //'nif_dueño',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Mascotas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'código' => $model->código]);
                 }
            ],
        ],
    ]); ?>


</div>
</div>
<?php else: ?>
    <?php
   $mensaje = '<div class="alert alert-warning" style="background-color: #E96363; color: white;">Debes iniciar sesión para tener acceso a esta página</div>';

    echo $mensaje;
?>
<?php endif; ?>

