<?php
/** @var yii\web\View $this */

/** @var string $content */
use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>


<!DOCTYPE html>

<head>


    <!--Nos va permitir insertar la imagen del logo en el menú-->
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/img/logo.png" type="image/x-icon" />

    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title> <!--Nos va a mostrar el título del hospital (PawPrints)-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <?php $this->head() ?>

</head>


<header>
    <!-- Dentro de tu archivo de layout -->


    <?php
    NavBar::begin([
        // 'brandLabel' => Html::img('@web/img/logo.png', ['alt'=>Yii::$app->name, 'class' => 'navbarimage','style' => 'height: 40px']),
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'], //Barra de navegación del menú
        'items' => [//Elementos que se van a mostrar en el menú
//            ['label' => 'Home', 'url' => ['/site/index']]
//            <div class="redes">



            ['label' => 'Servicios', 'url' => ['/site/about']], //Página Sobre servicios
            ['label' => 'Nosotros', 'url' => ['site/calculapeso']], //Página de nosotros
            ['label' => 'Contacto', 'url' => ['/site/contact']], //Página formulario de contacto  
            ['label' => 'Evaluación', 'url' => ['evaluaciones/index']],
//            ['label' => 'Registration', 'url' => ['site/registration']],
            ['label' => 'CRUDs', 'items' => [//Deplegable en el que se encuentran de forma organizada los CRUDS de la página
                    ['label' => 'Características', 'url' => ['caracteristicas/index']], //CRUD de características
                    ['label' => 'Evaluaciones', 'url' => ['evaluaciones/index']],
                    ['label' => 'Consultas', 'url' => ['consultas/index']], //CRUD de consultas
                    ['label' => 'Dueños', 'url' => ['duenos/index']], //CRUD de dueños
                    ['label' => 'Mascotas', 'url' => ['mascotas/index']], //CRUD de mascotas
                    ['label' => 'Pertenecen', 'url' => ['pertenecen/index']], //CRUD de pertenecen
                    ['label' => 'Productos y Accesorios', 'url' => ['productos-accesorios/index']], //CRUD de productos/accesorios
                    ['label' => 'Razas', 'url' => ['razas/index']], //CRUD de razas
                    ['label' => 'Teléfonos', 'url' => ['telefonos/index']], //CRUD de teléfonos
                    ['label' => 'Tienen', 'url' => ['tienen/index']], //CRUD de tienen
                    ['label' => 'Vacunas', 'url' => ['vacunas/index']], //CRUD de vacunas
                ], 'options' => ['class' => 'dropdown']], //Indica que es un desplegable de opciones
//            ['label' => 'Admin', 'url' => ['login']], //Página para crear cuenta
            Yii::$app->user->isGuest ? (
                    ['label' => 'Iniciar sesión', 'url' => ['/site/login']]
                    ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                    . Html::submitButton(
                            'Cerrar sesión (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
                    )
        ],
    ]);

    NavBar::end();
    ?>





</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
<?=
Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
])
?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-8 text-muted">
    <div class="container">
        <a class="a-custom" href="<?= Yii::$app->urlManager->createUrl(['site/politica']) ?>">Política de Privacidad</a>ㅤㅤ
        <a class="a-custom" href="<?= Yii::$app->urlManager->createUrl(['site/cookies']) ?>">Cookies</a><br>
        <p class="float-left">&copy; Paw Prints <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>

    </div>


</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
