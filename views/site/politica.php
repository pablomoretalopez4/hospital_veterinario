<?php
//

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'Política-Privacidad';

?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
<button style="background-color: white;" onclick="myFunction()"><div id="emojiContainer">&#x1F319;</div></button>




<script>
  var emojis = ["&#x1F319;", "&#x1F31E;"];
var currentEmojiIndex = 0;

function myFunction() {
     var element = document.body;
   element.classList.toggle("dark-mode");
  var emojiContainer = document.getElementById("emojiContainer");
  currentEmojiIndex = (currentEmojiIndex + 1) % emojis.length;
  emojiContainer.innerHTML = emojis[currentEmojiIndex];
 
}
</script>
<p style="text-align: justify;">
<center> <h2> POLÍTICA DE PRIVACIDAD</h2></center></br>
    La confidencialidad y la seguridad son valores primordiales de PAW PRINTS, en consecuencia, asumimos el compromiso de garantizar la privacidad del Usuario en todo momento y de no recabar información innecesaria. A continuación, le proporcionamos toda la información necesaria sobre nuestra Política de Privacidad en relación con los datos personales que recabamos, explicándole:</br></br>
<ul>
	<li>Quién es el responsable del tratamiento de sus datos.</li>
	<li>Para qué finalidades recabamos los datos que le solicitamos.</li>
        <li>Cuál es la legitimación para su tratamiento.</li>
        <li>Durante cuánto tiempo los conservamos.</li>
        <li>A qué destinatarios se comunican sus datos.</li>
        <li>Cuáles son sus derechos y cómo ejercerlos.</li>
        <li>Responsable: ver datos en el encabezamiento.</li>
</ul></br>


    <h4> <b>FINALIDADES, LEGITIMACIÓN Y CONSERVACIÓN de los tratamientos de los datos enviados a través de:</b></h4></br>

Formulario de Contacto.</br></br>

<b>Finalidad: </b>Facilitarle un medio para que pueda ponerse en contacto con nosotros y contestar a sus solicitudes de información, así como enviarle comunicaciones de nuestros productos, servicios y actividades, inclusive por medios electrónicos (correo electrónico, SMS, etc.), si marca la casilla de aceptación.</br></br>

<b>Legitimación:</b> El consentimiento del usuario al solicitarnos información a través de nuestro formulario de contactos y al marcar la casilla de aceptación de envío de información.</br></br>

<b>Conservación: </b>Una vez resuelta su solicitud por medio de nuestro formulario o contestada por correo electrónico, si no ha generado un nuevo tratamiento, y en caso de haber aceptado recibir envíos comerciales, hasta que solicite la baja de los mismos.</br></br>

Envío de correos electrónicos.</br></br>

<b>Finalidad: </b>Contestar a sus solicitudes de información, atender sus peticiones y responder sus consultas o dudas. En caso de recibir su Currículum Vitae, sus datos personales y curriculares podrán formar parte de nuestras bases de datos para participar en nuestros procesos de selección presentes y futuros.</br></br>

<b>Legitimación: </b>El consentimiento del usuario al solicitarnos información a través de la dirección de correo electrónico o enviarnos sus datos y CV para participar en nuestros procesos de selección.</br></br>

<b>Conservación:</b> Una vez resulta contestada su petición por correo electrónico, si no ha generado un nuevo tratamiento. En el caso de recibir su CV, sus datos podrán ser conservados durante un año máximo para futuros procesos de selección.</br></br>

Obligación de facilitarnos sus datos personales y consecuencias de no hacerlo.</br></br>

El suministro de datos personales requiere una edad mínima de 14 años o, en su caso, la edad mínima que establezca la normativa de protección de datos aplicable y/o disponer de capacidad jurídica suficiente para contratar.</br></br>

Los datos personales solicitados son necesarios para gestionar sus solicitudes, darle de alta como usuario y/o prestarle los servicios que pueda contratar, por lo que, si no nos los facilita, no podremos atenderle correctamente ni prestarle el servicio que ha solicitado.</br></br>

En todo caso, nos reservamos el derecho de decidir sobre la incorporación o no de sus datos personales y demás información a nuestras bases de datos.</br></br>

• Suscripción a nuestra newsletter</br></br>
<b>Finalidad:</b> Envío de nuestro boletín comercial y de comunicaciones informativas y publicitarias sobre nuestros productos o servicios que sean de su interés, incluso por medios electrónicos.</br></br>
<b>Legitimación:</b> El consentimiento del usuario al suscribirse a nuestros envíos comerciales y/o newsletters.</br></br>
<b>Conservación: </b>Hasta que el interesado solicite la baja de la suscripción a nuestros envíos comerciales.</br></br>

DESTINATARIOS DE SUS DATOS.</br>
Sus datos son confidenciales y no se cederán a terceros, salvo que exista obligación legal.</br></br>

COOKIES</br>
Esta página Web puede utilizar cookies pero que en ningún caso tratan datos de carácter personal, captan hábitos de navegación del usuario ni son utilizadas con fines publicitarios, y que por lo tanto quedan exceptuadas del cumplimiento de las obligaciones establecidas en el artículo 22 de la Ley de Servicios de la Sociedad de la Información. No obstante, el usuario consiente el uso de las cookies que nos permitan la comunicación entre el equipo del usuario y la red, prestar un servicio expresamente solicitado por el usuario, la autenticación o identificación de usuario (únicamente de sesión), dar seguridad al usuario, sesiones de reproductor multimedia, sesiones para equilibrar la carga, la personalización de la interfaz de usuario y de complementos (plug-in) e intercambiar contenidos sociales. De todas formas, el usuario si lo desea puede desactivar y/o eliminar estas cookies siguiendo las instrucciones de su navegador de Internet.</br></br>

DERECHOS EN RELACIÓN CON SUS DATOS PERSONALES.</br>
Cualquier persona puede retirar su consentimiento en cualquier momento, cuando el mismo se haya otorgado para el tratamiento de sus datos. En ningún caso, la retirada de este consentimiento condiciona la ejecución del contrato de suscripción o las relaciones generadas con anterioridad.</br></br>

Igualmente, puede ejercer los siguientes derechos:</br></br>

<ul>
	<li>Solicitar el acceso a sus datos personales o su rectificación cuando sean inexactos.</li>
	<li>Solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines para los que fueron recogidos.</li>
        <li>Solicitar la limitación de su tratamiento en determinadas circunstancias.</li>
        <li>Solicitar la oposición al tratamiento de sus datos por motivos relacionados con su situación particular.</li>
        <li>Solicitar la portabilidad de los datos en los casos previstos en la normativa.</li>
        <li>Otros derechos reconocidos en las normativas aplicables.</li>
        <li>Dónde y cómo solicitar sus Derechos: Mediante un escrito dirigido al responsable a su dirección postal o electrónica (indicadas en el apartado A), indicando la referencia “Datos Personales”, especificando el derecho que se quiere ejercer y respecto a qué datos personales.</li>
</ul></br>

En caso de divergencias con la empresa en relación con el tratamiento de sus datos, puede presentar una reclamación ante la Agencia de Protección de Datos (<a href=”www.agpd.es”>www.agpd.es</a>).</br></br>

SEGURIDAD DE SUS DATOS PERSONALES</br>
Con el objetivo de salvaguardar la seguridad de sus datos personales, le informamos que hemos adoptado todas las medidas de índole técnica y organizativa necesarias para garantizar la seguridad de los datos personales suministrados de su alteración, pérdida y tratamientos o accesos no autorizados.</br></br>

ACTUALIZACIÓN DE SUS DATOS</br>
Es importante que para que podamos mantener sus datos personales actualizados, nos informe siempre que haya habido alguna modificación en ellos, en caso contrario, no respondemos de la veracidad de los mismos.</br></br>

No nos hacemos responsables de la política de privacidad respecto a los datos personales que pueda facilitar a terceros por medio de los enlaces disponibles en nuestra página web.</br></br>

La presente Política de Privacidad ha sido modificada en fecha ___ y puede ser modificada para adaptarlas a los cambios que se produzca en nuestra web, así como modificaciones legislativas o jurisprudenciales sobre datos personales que vayan apareciendo, por lo que exige su lectura, cada vez que nos facilite sus datos a través de esta Web.</br></br>

D. RESPONSABILIDADES</br></br>

Al poner a disposición del usuario esta página Web queremos ofrecerle un servicio de calidad, utilizando la máxima diligencia en la prestación del mismo, así como en los medios tecnológicos utilizados. No obstante, no responderemos de la presencia de virus y otros elementos que de algún modo puedan dañar el sistema informático del usuario.</br></br>

No garantizamos que la disponibilidad del servicio sea continua e ininterrumpida.</br></br>

El USUARIO tiene prohibido cualquier tipo de acción sobre nuestro portal que origine una excesiva sobrecarga de funcionamiento a nuestros sistemas informáticos, así como la introducción de virus, o instalación de robots, o software que altere el normal funcionamiento de nuestra web, o en definitiva pueda causar daños a nuestros sistemas informáticos.</br></br>

El USUARIO asume toda la responsabilidad derivada del uso de nuestra página web.</br></br>

El USUARIO reconoce que ha entendido toda la información respecto a las condiciones de uso de nuestro portal, y reconoce que son suficientes para la exclusión del error en las mismas, y por lo tanto, las acepta integra y expresamente.</h5></br></br>

</p>
</body>
</html>