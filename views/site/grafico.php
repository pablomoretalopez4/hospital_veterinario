<?php

use dosamigos\chartjs\ChartJs;

// Obtener los datos desde la tabla "consultas"
$data = \app\models\Consultas::find()->select(['código', 'cantidad_pacientes'])->asArray()->all();

// Formatear los datos para que puedan ser utilizados por Chart.js
$labels = array_column($data, 'código');
$values = array_column($data, 'cantidad_pacientes');

// Configurar el gráfico
$chartData = [
    'type' => 'bar', // Tipo de gráfico (en este caso, barras)
    'data' => [
        'labels' => $labels,
        'datasets' => [
            [
                'label' => 'cantidad_pacientes', // Etiqueta para la serie de datos
                'backgroundColor' => '#4CAF50', // Color de fondo
                'data' => $values, // Valores de la serie de datos
            ],
        ],
    ],
    'options' => [
        'type' => 'bar', // Tipo de gráfico (bar, line, pie, etc.)
        'height' => 400, // Altura del gráfico
        'width' => 600, // Ancho del gráfico
    ],
];

// Mostrar el gráfico
echo ChartJs::widget([
    'data' => $chartData,
]);

