<?php
/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */

/** @var app\models\ContactForm $model */
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\captcha\Captcha;

$this->title = 'Contáctanos';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-contact">
    <div class="formulario"><h1><center><?= Html::encode($this->title) ?></h1></br>

        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <div class="alert alert-success">
                Gracias por contactar con nostotros, te responderemos con la mayor brevedad posible.
            </div>

            <p>
                Tenga en cuenta que si activa el depurador de Yii, debería poder
                para ver el mensaje de correo en el panel de correo del depurador.

                <?php if (Yii::$app->mailer->useFileTransport): ?>
                    Debido a que la aplicación está en modo de desarrollo, el correo electrónico no se envía sino que se guarda como
                    un archivo bajo <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                    Por favor configure el <code>useFileTransport</code> property of the <code>mail</code>
                    componente de la aplicación sea falso para habilitar el envío de correo electrónico.

                <?php endif; ?>
            </p>

        <?php else: ?>

            <p>
                Si tienes alguna duda respecto a nuestra aplicación web de hospital veterinario PawPrints. Contacte con nosotros. Gracias.
            </p>

            <div class="row">
                <div class="col-lg-11">

                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'subject') ?>

                    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

                    <?=
                    $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row "><div class="col-lg-4 ">{image}</div><div  class="col-lg-6" >{input}</div></div>',
                    ])
                    ?>
                    <label for="styled-checkbox" class="checkbox-label">
                        <h5> <input type="checkbox" id="styled-checkbox" class="styled-checkbox"> Acepto los <a href="http://localhost/hopital_veterinario/hospital_veterinario/web/index.php/site/politica" target="_blank" class="enlace-terminos">términos y condiciones</a> <br/></h5>
                    </label>
                    <div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div></br>


                    <h6><ul><li>RESPONSABLE: Pablo Moreta López.</br></li>
                            <li>FINALIDAD: Facilitarle la información que necesite o responder a su consulta.</br></li>
                            <li>LEGITIMACIÓN: Consentimiento del interesado.</br></li>
                            <li> DESTINATARIOS: No se cederán datos a terceros, salvo obligación legal.</br></li>
                            <li>DERECHOS: Acceder, rectificar y suprimir los datos, así como otros derechos, como se explica en la información adicional.</h6>  </li>          
                        </ul>
    <?php ActiveForm::end(); ?>

                </div>
            </div>

<?php endif; ?>
    </div>
</div>
