<?php
//

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'Calculadora';

?>
<!DOCTYPE html>
<html lang="en">

    <body>


        <section data-aos="fade-right" data-aos-duration="1500" class="titulo-pagina container-fluid">
            <div class="container align-content-center justify-content-center  ">

                <h2>Nosotros</h2>
            </div>
            <div class="redes">
                        <ul class="redes-list">   
                            <li class="redes-item"><a target="_blank" href="https://www.instagram.com/paw.prints/?hl=es"><i class="bi bi-instagram"></i></a></li> 
                            <li class="redes-item"><a target="_blank" href="https://www.facebook.com/ZoePawPrints/"><i class="bi bi-facebook"></i></a></li>
                            <li class="redes-item"><a target="_blank" href="https://api.whatsapp.com/send?phone=638323992"><i class="bi bi-whatsapp"></i></a></li>
                        </ul>
                    </div> 
        </section>

        <section class="bg-foto justify-content-center ">
        </section>

        <section class="contenido-flex">
            <section class="contenido-serv container-xl p-4">

                <div class="card p-0 m-4">
                    <div class="row g-2">
                        <div class="col-md-6">
                             <?= Html::img('@web/img/cita_previa.jpg', ['alt' => '...', 'class' => 'img-fluid rounded-start']) ?>
                         
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                               <h3 class="card-title line pb-3">Cita Previa</h3>
                                <h5 class="card-text">Para ser atendido se debe pedir cita previa en el teléfono 659 694 873, para poder coordinar la actividad quirúrgica y de consultas. Puede solicitarla para la consulta general o directamente para la consulta de especialista o pedir información.</h5>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="card p-0 m-4">
                    <div class="row g-2">
                        <div class="col-md-6 order-md-last">
                            <?= Html::img('@web/img/urgencias.png', ['alt' => '...', 'class' => 'img-fluid rounded-start']) ?>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <h3 class="card-title line pb-3">Urgencias</h3>
                                <h5 class="card-text">El Hospital Veterinario PawPrints dispone de un servicio de Urgencias 24 horas todos los días del año llamando al teléfono 562 48 94 58.
El veterinario se acerca al domicilio del cliente para recoger la mascota.</h5>               

                            </div>
                        </div>
                    </div>
                </div>

                <div class="card p-0 m-4">
                    <div class="row g-2">
                        <div class="col-md-6">
                           <?= Html::img('@web/img/analisis.jpg', ['alt' => '...', 'class' => 'img-fluid rounded-start']) ?>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <h3 class="card-title line pb-3" >Análisis clínico</h3>
                                <h5 class="card-text">Un pronto diagnóstico en su mascota es importante, contamos con un laboratorio de análisis clínicos propio, en el que realizamos la mayoría de las analíticas de una manera inmediata. </h5>      
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card p-0 m-4">
                    <div class="row g-2">
                        <div class="col-md-6 order-md-last">
                           <?= Html::img('@web/img/maquinas.jpeg', ['alt' => '...', 'class' => 'img-fluid rounded-start']) ?>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <h3 class="card-title line pb-3">Equipos médicos</h3>
                                <h5 class="card-text">Para disponer de las últimas técnicas en diagnóstico por imagen PawPrints dispone de diferentes equipos y medios que nos ayudan a valorar mejor a su animal. Contamos con una amplia sala perfectamente acondicionada para todo tipo de animales.</h5>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card p-0 m-4">
                    <div class="row g-2">
                        <div class="col-md-6">
                                      <center><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2900.164450171406!2d-3.2102476845112777!3d43.37358537913191!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4ef3cf7e5706d1%3A0xc1c8147aafa404c2!2sAv.%20la%20Playa%2C%209%2C%2039700%20Castro-Urdiales%2C%20Cantabria!5e0!3m2!1ses!2ses!4v1682177637811!5m2!1ses!2ses" width="500" height="330" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></center>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <h3 class="card-title line pb-3">Puedes encontrarnos en:</h3>
                                <h5 class="card-text">Av. la Playa, 9, 39700 Castro-Urdiales, Cantabria. </h5>

                            </div>
                        </div>
                    </div>
                </div>
             

            </section>
            
        </section>




    </body>
  
</html>



