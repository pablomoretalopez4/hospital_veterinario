
<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hospital_veterinario";

$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar la conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

// Procesar el formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Recuperar los datos del formulario
    $nombre = $_POST["nombre"];
    $puntuacion = $_POST["puntuacion"];
    $comentario = $_POST["comentario"];

    // Insertar los datos en la base de datos
    $sql = "INSERT INTO evaluaciones (nombre, puntuacion, comentario) VALUES ('$nombre', '$puntuacion', '$comentario')";

    if ($conn->query($sql) === TRUE) {
        echo "Evaluación creada exitosamente";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

// Cerrar la conexión a la base de datos
$conn->close();
?>
