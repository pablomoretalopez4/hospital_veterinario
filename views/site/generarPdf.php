
<?php

use app\models\Consultas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */


?>
<div id="fondo">
<div class="consultas-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center><br>

    <p>
        <?= Html::a('Crear Consultas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
           /** ['class' => 'yii\grid\SerialColumn'],*/

            'código',
            'cantidad_pacientes',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Consultas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'código' => $model->código]);
                 }
            ],
        ],
    ]); ?>


</div>
    </div>
