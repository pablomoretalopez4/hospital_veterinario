<?php


// Conexión a la base de datos
$conexion = mysqli_connect("localhost", "root", "", "hospital_veterinario");

// Consulta para obtener las evaluaciones y reseñas
$query = "SELECT * FROM evaluaciones ORDER BY fecha DESC";
$resultado = mysqli_query($conexion, $query);

// Mostrar las evaluaciones y reseñas
while ($fila = mysqli_fetch_assoc($resultado)) {
    echo "<p><strong>{$fila['nombre']}</strong></p>";
    echo "<p>Puntuación: {$fila['puntuacion']}</p>";
    echo "<p>Comentario: {$fila['comentario']}</p>";
    echo "<hr>";
}

// Cerrar conexión a la base de datos
mysqli_close($conexion);
?>