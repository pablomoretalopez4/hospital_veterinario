<?php
//

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'Servicios';
?>
<!DOCTYPE html>
<html lang="en">

    <body>


        <section data-aos="fade-right" data-aos-duration="1500" class="titulo-pagina container-fluid">
            <div class="container align-content-center justify-content-center  ">

                <h2>Servicios</h2>
            </div>
        </section>

        <section class="bg-foto justify-content-center ">
        </section>

        <section class="contenido-flex">
            <section class="contenido-serv container-xl p-4">

                <div class="card p-0 m-4">
                    <div class="row g-2">
                        <div class="col-md-6">
                             <?= Html::img('@web/img/laserterapia.jpg', ['alt' => '...', 'class' => 'img-fluid rounded-start']) ?>
                         
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <h3 class="card-title line pb-3">Laserterapia</h3>
                                <h5 class="card-text">El hospital veterinario PawPrints ofrece la especialidad de oncología desde hace años. Según los estudios, uno de cada cuatro perros contraerá cáncer en su vida.</h5>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="card p-0 m-4">
                    <div class="row g-2">
                        <div class="col-md-6 order-md-last">
                            <?= Html::img('@web/img/ecografia.jpg', ['alt' => '...', 'class' => 'img-fluid rounded-start']) ?>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <h3 class="card-title line pb-3">Ecografía</h3>
                                <h5 class="card-text">En el hospital veterinario contamos con todo el equipamiento necesario para realizar ecocardiografías, auxilio a biopsias y ultrasonografía ocular, articular y abdominal.</h5>               

                            </div>
                        </div>
                    </div>
                </div>

                <div class="card p-0 m-4">
                    <div class="row g-2">
                        <div class="col-md-6">
                           <?= Html::img('@web/img/oncologia.jpg', ['alt' => '...', 'class' => 'img-fluid rounded-start']) ?>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <h3 class="card-title line pb-3" >Oncología</h3>
                                <h5 class="card-text">Ofrecemos la especialidad de oncología desde hace años. Según los estudios, uno de cada cuatro perros contraerá cáncer en su vida. ​Al igual que con los humanos, el cáncer priva de años de vida.</h5>      
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card p-0 m-4">
                    <div class="row g-2">
                        <div class="col-md-6 order-md-last">
                           <?= Html::img('@web/img/odontologia.jpg', ['alt' => '...', 'class' => 'img-fluid rounded-start']) ?>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <h3 class="card-title line pb-3">Odontología</h3>
                                <h5 class="card-text">En el hospital veterinario PawPrints ofrecemos la especialidad de odontología de la mano de uno de los especialistas más reconocidos en el sector.</h5>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card p-0 m-4">
                    <div class="row g-2">
                        <div class="col-md-6">
                          <?= Html::img('@web/img/laparoscopia.jpg', ['alt' => '...', 'class' => 'img-fluid rounded-start']) ?>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <h3 class="card-title line pb-3">Laparoscopia</h3>
                                <h5 class="card-text">Ofrecemos cirugía de mínima invasión de la mano de uno de los más reconocidos especialistas en laparoscopia y endoscopia. </h5>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card p-0 m-4">
                    <div class="row g-2">
                        <div class="col-md-6 order-md-last">
                            <?= Html::img('@web/img/medicina_interna.jpg', ['alt' => '...', 'class' => 'img-fluid rounded-start']) ?>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <h3 class="card-title line pb-3">Medicina Interna</h3>
                                <h5 class="card-text">Fue pionero en el uso de la endoscopia digestiva en pequeños animales y es además referencia en cirugía de mínima invasión (laparoscópica) y endoscopia, recibiendo casos referidos de distintas zonas de Cantabria y también del resto de España.</h5>

                            </div>
                        </div>
                    </div>
                </div>

            </section>
            
        </section>




    </body>
  
</html>