
<?php
//

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'Paw Prints';
?>
<script>
    window.addEventListener('load', function () {
        // Establecer el tiempo (en milisegundos) durante el cual se mostrará el preloader
        var tiempoPreloader = 3000; // Cambia esto al tiempo deseado

        // Ocultar el preloader después de que haya pasado el tiempo establecido
        setTimeout(function () {
            document.getElementById('preloader').style.display = 'none';
        }, tiempoPreloader);
    });

//    Mostrar popup en la index
    function mostrarPopup() {
        var popup = document.getElementById("miPopup");
        popup.style.display = "block";

        var botonCerrar = document.getElementById("cerrar");
        botonCerrar.addEventListener("click", function () {
            popup.style.display = "none";
        });
    }

    function mostrarPopup2() {
        var popup = document.getElementById("miPopup2");
        popup.style.display = "block";

        var botonCerrar = document.getElementById("cerrar2");
        botonCerrar.addEventListener("click", function () {
            popup.style.display = "none";
        });
    }

    function mostrarPopup3() {
        var popup = document.getElementById("miPopup3");
        popup.style.display = "block";

        var botonCerrar = document.getElementById("cerrar3");
        botonCerrar.addEventListener("click", function () {
            popup.style.display = "none";
        });
    }


    function mostrarPopup4() {
        var popup = document.getElementById("miPopup4");
        popup.style.display = "block";

        var botonCerrar = document.getElementById("cerrar4");
        botonCerrar.addEventListener("click", function () {
            popup.style.display = "none";
        });
    }


    function mostrarPopup5() {
        var popup = document.getElementById("miPopup5");
        popup.style.display = "block";

        var botonCerrar = document.getElementById("cerrar5");
        botonCerrar.addEventListener("click", function () {
            popup.style.display = "none";
        });
    }


    function mostrarPopup6() {
        var popup = document.getElementById("miPopup6");
        popup.style.display = "block";

        var botonCerrar = document.getElementById("cerrar6");
        botonCerrar.addEventListener("click", function () {
            popup.style.display = "none";
        });
    }


    function mostrarPopup7() {
        var popup = document.getElementById("miPopup7");
        popup.style.display = "block";

        var botonCerrar = document.getElementById("cerrar7");
        botonCerrar.addEventListener("click", function () {
            popup.style.display = "none";
        });
    }


    function mostrarPopup8() {
        var popup = document.getElementById("miPopup8");
        popup.style.display = "block";

        var botonCerrar = document.getElementById("cerrar8");
        botonCerrar.addEventListener("click", function () {
            popup.style.display = "none";
        });
    }


</script>




















<section class="contenido-flex container-xxl">
    <div class="row text-center pt-4">
        <div class="col-12 text-prod">

            <h1>Paw Prints</h1>


        </div>
    </div>
</section>

<section class="contenido-flex">
    <section class="contenido container-xl prod">
        <div class="tarjeta-productos"> 
            <div class="container text-center justify-content-center align-content-center">
                <div class="row pt-4 pb-4 px-3 g-4">
                    <div class="col-md-6">
                        <div class="card" data-aos="zoom-in-up">
                            <img src="img/teleconsultaa.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title"></h5>
                                <p class="card-text">Teleconsulta de Perros</p>
                                <button onclick="mostrarPopup()">Ir al servicio</button>

                                <div id="miPopup" class="popup">
                                    <h6>Se pondrá en contacto contigo Roberto López, responsable del servicio de medicina interna en el Hospital Veterinario Paw Prints, en un plazo máximo de 30 minutos.</h6>
                                    <button id="cerrar">Cerrar</button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card" data-aos="zoom-in-up">
                            <img src="img/gato.jpeg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title"></h5>
                                <p class="card-text">Teleconsulta de gatos</p>
                                <button onclick="mostrarPopup2()">Ir al servicio</button>

                                <div id="miPopup2" class="popup">
                                    <h6>Se pondrá en contacto contigo Susana Díaz, responsable del servicio de medicina felina y animales exóticos en el Hospital Veterinario Paw Prints, en un plazo máximo de 30 minutos.</h6>
                                    <button id="cerrar2">Cerrar</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="container text-center justify-content-center align-content-center">
                <div class="row  pb-4 px-3 g-4 g-sm-4 gx-sm-4">
                    <div class="col-md-3">
                        <div class="card h-100" data-aos="zoom-in-up">
                            <img src="img/equipo_3.jpeg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Conoce a nuestro equipo</h5>
                                <p class="card-text">Doctora Silvia Moreno</p>
                                <button onclick="mostrarPopup3()">Ver más</button>

                                <div id="miPopup3" class="popup">
                                    <h6>Licenciada en la Facultad de Zaragoza, realizó su internado en el Hospital de la Facultad de Zaragoza, realizó los dos másters propios I y II. En 2016 obtuvo el tí­tulo de postgrado en oftalmología en la UAB.
                                    </h6>
                                    <button id="cerrar3">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card h-100" data-aos="zoom-in-up">
                            <img src="img/equipo_4.jpeg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Conoce a nuestro equipo</h5>
                                <p class="card-text">Doctora Susana Díaz</p>
                                <button onclick="mostrarPopup4()">Ver más</button>

                                <div id="miPopup4" class="popup">
                                    <h6>Profesional con alta implicación y cariño por su trabajo. Responsable del servicio de medicina y cirugía de animales exóticos y responsable del servicio de medicina felina. Licenciada por la Universidad de León. </h6>
                                    <button id="cerrar4">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card h-100" data-aos="zoom-in-up">
                            <img src="img/equipo_2.jpeg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Conoce a nuestro equipo</h5>
                                <p class="card-text">Doctora Yaiza Gómez</p>
                                <button onclick="mostrarPopup5()">Ver más</button>

                                <div id="miPopup5" class="popup">
                                    <h6>Directora General del Hospital. Licenciada en la Facultad de Veterinaria de León. Diplomada en oftalmologí­a por la UAB. En París fragua algunas de sus especialidades como la neurocirugía, la traumatología o la oftalmología.  </h6>
                                    <button id="cerrar5">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card h-100" data-aos="zoom-in-up">
                            <img src="img/equipo_1.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Conoce a nuestro equipo</h5>
                                <p class="card-text">Doctor Roberto López</p>
                                <button onclick="mostrarPopup6()">Ver más</button>

                                <div id="miPopup6" class="popup">
                                    <h6>Gran cirujano. Responsable de los servicios de cirugía de tejidos blandos, traumatologí­a y ortopedia en nuestro Hospital. Licenciado en la UAX. Ha realizado másters en cirugía relacionada con las vías respiratorias y cirugí­a láser.   </h6>
                                    <button id="cerrar6">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container text-center justify-content-center align-content-center">
                    <div class="row  pb-4 px-3 g-4">
                        <div class="col-md-12">
                            <div class="card h-100" data-aos="zoom-in-up">



                                <img src="img/plano.jpg" class="card-img-top" alt="...">



                                <div class="card-body">
                                    <h5 class="card-title">MAPA</h5>
                                    <p class="card-text">Nuestro Hospital desde una vista 2D</p>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
</section>

<!--pantalla de precarga de la página-->

<div id="preloader">
    <div id="fondoprecarga">
        <img src="img/precarga2-unscreen.gif" class="precarga" alt="precarga">
    </div>
</div>









