<?php
//

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'Cookies';

?>
   
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
    

<button style="background-color: white;" onclick="myFunction()"><div id="emojiContainer">&#x1F319;</div></button>




<script>
  var emojis = ["&#x1F319;", "&#x1F31E;"];
var currentEmojiIndex = 0;

function myFunction() {
     var element = document.body;
   element.classList.toggle("dark-mode");
  var emojiContainer = document.getElementById("emojiContainer");
  currentEmojiIndex = (currentEmojiIndex + 1) % emojis.length;
  emojiContainer.innerHTML = emojis[currentEmojiIndex];
}


</script>
<p style="text-align: justify;">
<center><h2> Política de Cookies </h2></center> </br> 
Esta declaración de cookies fue actualizada por última vez en 10 de marzo de 2021 y se aplica a los ciudadanos del Espacio Económico Europeo. </br> </br>

<ol>
    <li><b>Introducción</b></br>Nuestro sitio web, WWW.HOSPITALPRIVET.COM (en adelante: “el sitio web”) utiliza cookies y otras tecnologías relacionadas (para mayor comodidad, todas las tecnologías se denominan “cookies”). Las cookies también son colocadas por terceros que hemos contratado. </br> En el siguiente documento le informamos sobre el uso de cookies en nuestro sitio web.</li></br>
    <li><b>¿Qué son las cookies?</b></br>Una cookie es un pequeño archivo simple que se envía junto con las páginas de este sitio web y que su navegador almacena en el disco duro de su ordenador o de otro dispositivo. La información almacenada puede ser devuelta a nuestros servidores o a los servidores de los terceros relevantes durante una visita posterior.</li></br>
    <li><b>¿Qué son los scripts?</b></br>Un script es un fragmento de código de programa que se utiliza para hacer que nuestro sitio web funcione correctamente y de forma interactiva. Este código se ejecuta en nuestro servidor o en su dispositivo.</li> </br>
    <li><b>¿Qué es una baliza web?</b></br>Una baliza web (o una etiqueta de píxel) es una pequeña e invisible pieza de texto o imagen en un sitio web que se utiliza para monitorear el tráfico en un sitio web. Para ello, se almacenan varios datos sobre usted mediante web beacons.</li></br>
    <li><b>Consentimiento</b></br>Cuando visite nuestro sitio web por primera vez, le mostraremos una ventana emergente con una explicación sobre las cookies. Tan pronto como haga clic en “Guardar preferencias”, nos autoriza a utilizar las categorías de cookies y plug-ins que seleccionó en la ventana emergente, tal y como se describe en esta declaración de cookies. Usted puede desactivar el uso de cookies a través de su navegador, pero tenga en cuenta que es posible que nuestro sitio web ya no funcione correctamente. </br>Administración de configuración de consentimiento. El usuario tiene el derecho de mostrar su conformidad a través de la prestación del consentimiento que solicitaremos al visitar nuestra web. Dicho consentimiento podrá ser modificado en todo momento.</li></br>
    <li><b>Cookies</b></br><ol><b>6.1 Cookies técnicas o funcionales</b></br>Algunas cookies aseguran que ciertas partes del sitio web funcionen correctamente y que sus preferencias de usuario sigan siendo conocidas. Al colocar cookies funcionales, le facilitamos la visita a nuestro sitio web. De esta manera, usted no necesita introducir repetidamente la misma información cuando visita nuestro sitio web y, por ejemplo, los artículos permanecen en su cesta de la compra hasta que usted haya pagado. Podemos colocar estas cookies sin su consentimiento.</ol></li>
    </br><ol><b>6.2 Cookies analíticas </b></br>
    Son aquéllas que bien tratadas por nosotros o por terceros, nos permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios del servicio ofertado. Para ello se analiza su navegación en nuestra página web con el fin de mejorar la oferta de productos o servicios que le ofrecemos. </br></ol> </li>
    </br><ol><b>6.3 Cookies de publicidad </b></br>
    Son aquéllas que, bien tratadas por nosotros o por terceros, nos permiten gestionar de la forma más eficaz posible la oferta de los espacios publicitarios que hay en la página web, adecuando el contenido del anuncio al contenido del servicio solicitado o al uso que realice de nuestra página web. Para ello podemos analizar sus hábitos de navegación en Internet y podemos mostrarle publicidad relacionada con su perfil de navegación. </ol></li> </br>
    <li><b>Sus derechos con respecto a los datos personales </b></br>Usted tiene los siguientes derechos con respecto a sus datos personales: Usted tiene derecho a saber por qué se necesitan sus datos personales, qué sucederá con ellos y durante cuánto tiempo se conservarán.
</br>Derecho de acceso: Usted tiene derecho a acceder a sus datos personales que conocemos.
</br>Derecho de rectificación: tiene derecho a completar, rectificar, borrar o bloquear sus datos personales cuando lo desee.
</br>Si usted nos da su consentimiento para procesar sus datos, tiene derecho a revocar dicho consentimiento y a que se eliminen sus datos personales.
</br>Derecho de cesión de sus datos: tiene derecho a solicitar todos sus datos personales al responsable del tratamiento y a transferirlos íntegramente a otro responsable del tratamiento.
</br>Derecho de oposición: usted puede oponerse al tratamiento de sus datos. Nosotros cumplimos con esto, a menos que existan motivos justificados para el procesamiento.
</br>Para ejercer estos derechos, póngase en contacto con nosotros. Por favor refiérase a los detalles de contacto en la parte inferior de esta declaración de cookies. Si tiene alguna queja sobre cómo manejamos sus datos, nos gustaría saber de usted, pero también tiene derecho a presentar una queja a la autoridad supervisora (la Autoridad de Protección de Datos).</li> </br>
    <li><b>Habilitación/deshabilitación y eliminación de cookies </b></br>Puede utilizar su navegador de Internet para eliminar las cookies de forma automática o manual. Usted también puede especificar que ciertas cookies no pueden ser colocadas. Otra opción es cambiar la configuración de su navegador de Internet para que reciba un mensaje cada vez que se coloca una cookie. Para obtener más información sobre estas opciones, consulte las instrucciones de la sección Ayuda de su navegador.
Tenga en cuenta que nuestro sitio web puede no funcionar correctamente si todas las cookies están deshabilitadas. Si borra las cookies de su navegador, se volverán a colocar después de su consentimiento cuando vuelva a visitar nuestros sitios web.
Aquí puede consultar la ayuda de los principales navegadores Web para cancelar o autorizar una cookie concreta:</br> </br>
<b>GOOGLE CHROME</b> </br><a href=”https://support.google.com/chrome/answer/95647?hl=es”>https://support.google.com/chrome/answer/95647?hl=es</a> </br></br>
<b>MOZILLA FIREFOX</b> </br><a href=”https://support.mozilla.org/es/kb/habilitar‐y‐deshabilitar‐cookies‐que‐los‐sitios‐we”>https://support.mozilla.org/es/kb/habilitar‐y‐deshabilitar‐cookies‐que‐los‐sitios‐we</a>  </br></br>
<b>INTERNET EXPLORER</b> </br><a href=”http://windows.microsoft.com/es‐es/windows‐vista/block‐or‐allow‐cookies”>http://windows.microsoft.com/es‐es/windows‐vista/block‐or‐allow‐cookies</a></br></br>
<b>SAFARI </b></br><a href=”https://support.apple.com/kb/PH17191?locale=es_ES”>https://support.apple.com/kb/PH17191?locale=es_ES</a></br></br>
<b>OPERA </b></br><a href=”http://help.opera.com/Windows/12.10/en/cookies.html”>http://help.opera.com/Windows/12.10/en/cookies.html</a></br></br>
<b>WINDOWS PHONE</b></br> <a href=”http://www.windowsphone.com/es‐es/how‐to/wp7/web/changing‐privacy‐and‐other‐browsersettings</br>http://www.windowsphone.com/es‐es/how‐to/wp7/web/changing‐privacy‐and‐other‐browsersettings</br></a></br>
<b>IPHONE & IPAD </b></br><a href=”https://support.apple.com/en‐us/HT201265”>https://support.apple.com/en‐us/HT201265</a></br></br>
<b>CHROME ANDROID</b></br><a href=”https://support.google.com/chrome/answer/2392971?hl=es”>https://support.google.com/chrome/answer/2392971?hl=es</a> </li> </br>
    <li><b>Detalles de contacto </b></br>Para preguntas y/o comentarios sobre nuestra política de cookies y esta declaración, póngase en contacto con nosotros utilizando los siguientes datos de contacto:</br>
</br><center><b>Denominación:</b> HOSPITAL VETERINARIO PAW PRINTS.</br>
<b>Dirección:</b> Av. la Playa, 9, 39700 Castro-Urdiales, Cantabria.</br>
<b>Website:</b> www.hospivetpawprints.com.</br>
<b>Email:</b> infopawprints@gmail.com.</br>
<b>Número de teléfono:</b> 650993428. </center></li></br>
</ol>


</p>
</body>
</html>


