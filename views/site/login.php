<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var app\models\LoginForm $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;




?>

  

    

    
 
     <?php

    $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-horizontal'],
]) 


    ?>
<div class="formulario-contacto">
        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>
    <!--<p> <?= Html::a('¿Has olvidado la Contraseña?',['recoverpass' ]) ?> </p>-->
        

        <div class="form-group">
            <div class="offset-lg-1 col-lg-11">
                <?= Html::submitButton('Iniciar sesión', ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
           <?= Html::resetButton('Borrar', ['class' => 'btn btn-secondary btn-default']) ?>
            </div>
        

        </div>

    <?php ActiveForm::end(); ?>

  

