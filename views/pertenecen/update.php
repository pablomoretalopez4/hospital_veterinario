<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Pertenecen $model */

$this->title = 'Actualizar Pertenecen: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pertenecens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pertenecen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
