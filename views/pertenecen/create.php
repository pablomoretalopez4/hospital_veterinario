<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Pertenecen $model */

$this->title = 'Crear Pertenecen';
$this->params['breadcrumbs'][] = ['label' => 'Pertenecens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>




   <div class="pertenecen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
       
    ]) ?>

</div>

