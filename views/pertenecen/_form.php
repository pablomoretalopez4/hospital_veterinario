<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/** @var yii\web\View $this */
/** @var app\models\Pertenecen $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pertenecen-form">

    <?php $form = ActiveForm::begin(); ?>

  <?= $form->field($model, 'código_mascota')->dropDownList(
        ArrayHelper::map(app\models\Mascotas::find()->all(), 'código', 'código'),
        ['prompt'=>'Selecciona una mascota']
    );
?>
    <?= $form->field($model, 'id_raza')->dropDownList(
        ArrayHelper::map(app\models\Razas::find()->all(), 'id', 'id'),
        ['prompt'=>'Selecciona una raza']
    );
?>

    
   
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
