<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use kartik\mpdf\Pdf;
use app\models\PasswordResetRequestForm;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    public function actionRegister() {
        //Creamos la instancia con el model de validación
        $model = new FormRegister;

        //Mostrará un mensaje en la vista cuando el usuario se haya registrado
        $msg = null;

        //Validación mediante ajax
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        //Validación cuando el formulario es enviado vía post
        //Esto sucede cuando la validación ajax se ha llevado a cabo correctamente
        //También previene por si el usuario tiene desactivado javascript y la
        //validación mediante ajax no puede ser llevada a cabo
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                //Preparamos la consulta para guardar el usuario
                $table = new Users;
                $table->username = $model->username;
                $table->email = $model->email;
                //Encriptamos el password
                $table->password = crypt($model->password, Yii::$app->params["salt"]);
                //Creamos una cookie para autenticar al usuario cuando decida recordar la sesión, esta misma
                //clave será utilizada para activar el usuario
                $table->authKey = $this->randKey("abcdef0123456789", 200);
                //Creamos un token de acceso único para el usuario
                $table->accessToken = $this->randKey("abcdef0123456789", 200);

                //Si el registro es guardado correctamente
                if ($table->insert()) {
                    //Nueva consulta para obtener el id del usuario
                    //Para confirmar al usuario se requiere su id y su authKey
                    $model->username = null;
                    $model->email = null;
                    $model->password = null;
                    $model->password_repeat = null;

                    $msg = Yii::$app->getSession()->setFlash('success', 'Enhorabuena, ya esta creado tu usuario.');
                    return $this->redirect(["site/index"]);
                } else {
                    $msg = "Ha ocurrido un error al llevar a cabo tu registro";
                }
            } else {
                $model->getErrors();
            }
        }
        return $this->render("register", ["model" => $model, "msg" => $msg]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
 public function actionContact()
{
    $model = new ContactForm();
    $email = 'hospitalpawprints@gmail.com'; 

    if ($model->load(Yii::$app->request->post()) && $model->contact($email)) {
        Yii::$app->session->setFlash('contactFormSubmitted');
        return $this->refresh();
    }

    return $this->render('contact', [
        'model' => $model,
    ]);
}

    


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
   
    
    
     public function actionCalculapeso()
    {
        return $this->render('calculapeso');
    }
    
    public function actionPolitica()
    {
        return $this->render('politica');
    }
    
     public function actionCookies()
    {
        return $this->render('cookies');
    }
    
       public function actionRegistration()
    {
        return $this->render('registration');
    }
    
       public function actionContacto()
    {
        return $this->render('contacto');
    }
       public function actionEvaluaciones()
    {
        return $this->render('evaluaciones');
    }
       public function actionNueva_evaluacion()
    {
        return $this->render('nueva_evaluacion');
    }
      public function actionGraficos()
    {
        return $this->render('graficos');
    }
       public function actionRegclien()
    {
        return $this->render('regclien');
    }
    
    public function actionGenerarPdf()
{
    // Renderiza la vista
    $content = $this->renderPartial('consultas/index');
    
    // Establece las opciones para el módulo mpdf
    $pdf = new Pdf([
        // Configura el tamaño y la orientación del papel
        'format' => Pdf::FORMAT_A4,
        'orientation' => Pdf::ORIENT_PORTRAIT,
        // Establece las opciones de margen
        'marginLeft' => 5,
        'marginRight' => 5,
        'marginTop' => 10,
        'marginBottom' => 10,
        // Establece las opciones del encabezado y pie de página
        'options' => [
            'title' => 'Tu título aquí',
            'subject' => 'Tu subtítulo aquí',
            'keywords' => 'tus, palabras, clave, aquí',
        ],
    ]);
    
    // Configura el contenido del PDF y devuelve una respuesta para descargarlo
    $pdf->content = $content;
    return $pdf->render();
}

public function actionRequestPasswordReset()
{
    $model = new PasswordResetRequestForm();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        if ($model->sendEmail()) {
            Yii::$app->session->setFlash('success', 'Se ha enviado un correo electrónico con las instrucciones para restablecer tu contraseña.');
            return $this->goHome();
        } else {
            Yii::$app->session->setFlash('error', 'Lo sentimos, no se pudo enviar el correo electrónico de recuperación de contraseña. Por favor, inténtalo más tarde.');
        }
    }

    return $this->render('requestPasswordReset', [
        'model' => $model,
    ]);
}


public function actionGrafico()
{
    return $this->render('grafico');
}







    
    
   
     
    
    


    
    
}
