<?php

namespace app\controllers;

use app\models\Duenos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DuenosController implements the CRUD actions for Duenos model.
 */
class DuenosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Duenos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Duenos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'nif' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Duenos model.
     * @param string $nif Nif
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($nif)
    {
        return $this->render('view', [
            'model' => $this->findModel($nif),
        ]);
    }

    /**
     * Creates a new Duenos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Duenos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save(false)) {
                return $this->redirect(['view', 'nif' => $model->nif]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Duenos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nif Nif
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nif)
    {
        $model = $this->findModel($nif);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nif' => $model->nif]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Duenos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nif Nif
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nif)
    {
        $this->findModel($nif)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Duenos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nif Nif
     * @return Duenos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nif)
    {
        if (($model = Duenos::findOne(['nif' => $nif])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
